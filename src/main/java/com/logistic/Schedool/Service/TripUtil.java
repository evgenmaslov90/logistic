package com.logistic.Schedool.Service;

import com.logistic.Schedool.Entity.Trip;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
public class TripUtil {

    private String carrier;
    private String startCity;
    private String endCity;
    private List<String> sortingCriteria;

    //Cоздание нужного пути
    public TripUtil(String carrier, String startCity, String endCity, List<String> sortingCriteria) {
        this.carrier = carrier;
        this.startCity = startCity;
        this.endCity = endCity;
        this.sortingCriteria = sortingCriteria;
    }

    public List<Trip> prioritizeAndSortTrips(List<Trip> trips) {
        return trips.stream()
                .filter(this::isTripMatchingConditions)
                .sorted(this::compareTrips)
                .collect(Collectors.toList());
    }

    private boolean isTripMatchingConditions(Trip trip) {
        return trip.getCarrierName().equals(carrier)
                && trip.getStartCity().equals(startCity)
                && trip.getEndCity().equals(endCity);
    }

    private int compareTrips(Trip trip1, Trip trip2) {

        for (String criteria : sortingCriteria) {
            int result = compareByCriteria(trip1, trip2, criteria);
            if (result != 0) {
                return result;
            }
        }
        return 0; // Все критерии сравнения равны, считаем объекты равными
    }

    private int compareByCriteria(Trip trip1, Trip trip2, String criteria) {
        switch (criteria) {
            case "price":
                return Double.compare(trip1.getPrice(), trip2.getPrice());
            case "startTime":
                return trip1.getStartTime().compareTo(trip2.getStartTime());
            case "endTime":
                return trip1.getEndTime().compareTo(trip2.getEndTime());
            default:
                throw new IllegalArgumentException("Unknown sorting criteria: " + criteria);
        }
    }
}



