package com.logistic.Schedool.Service;

import com.logistic.Schedool.Entity.Trip;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
//Создание изначальных поездок
public abstract class CreatingTrips {

    public static List<Trip> createTrips() {
        List<Trip> trips = new ArrayList<>();
        // Добавьте рейсы с пересекающимися значениями полей
        trips.add(new Trip("1", "City A", "City B", 100.0,
                LocalDateTime.of(2023, 5, 21, 9, 0),
                LocalDateTime.of(2023, 5, 21, 18, 0),
                "IP_Katkovskie"));
        trips.add(new Trip("2", "City A", "City C", 150.0,
                LocalDateTime.of(2023, 5, 21, 12, 0),
                LocalDateTime.of(2023, 5, 22, 5, 0),
                "IP_Kazimir"));
        trips.add(new Trip("3", "City B", "City A", 100.0,
                LocalDateTime.of(2023, 5, 22, 3, 0),
                LocalDateTime.of(2023, 5, 22, 12, 0),
                "IP_Vlados"));
        trips.add(new Trip("4", "City C", "City A", 50.0,
                LocalDateTime.of(2023, 5, 22, 3, 0),
                LocalDateTime.of(2023, 5, 23, 12, 0),
                "IP_Ivanna"));
        trips.add(new Trip("5", "City B", "City C", 40.0,
                LocalDateTime.of(2023, 5, 21, 10, 0),
                LocalDateTime.of(2023, 5, 21, 22, 0),
                "IP_Den"));
        trips.add(new Trip("6", "City C", "City B", 250.0,
                LocalDateTime.of(2023, 5, 22, 3, 0),
                LocalDateTime.of(2023, 5, 22, 15, 0),
                "IP_Katkovskie"));
        trips.add(new Trip("7", "City A", "City B", 350.0,
                LocalDateTime.of(2023, 5, 21, 11, 0),
                LocalDateTime.of(2023, 5, 21, 15, 0),
                "IP_Lena"));
        trips.add(new Trip("8", "City B", "City A", 50.0,
                LocalDateTime.of(2023, 5, 22, 13, 0),
                LocalDateTime.of(2023, 5, 24, 13, 0),
                "IP_Vlados"));
        trips.add(new Trip("9", "City C", "City A", 650.0,
                LocalDateTime.of(2023, 5, 20, 12, 0),
                LocalDateTime.of(2023, 5, 20, 15, 0),
                "IP_White"));
        trips.add(new Trip("10", "City B", "City C", 10.0,
                LocalDateTime.of(2023, 5, 22, 7, 0),
                LocalDateTime.of(2023, 5, 28, 13, 0),
                "IP_Mexico"));
        trips.add(new Trip("11", "City A", "City C", 2.0,
                LocalDateTime.of(2023, 5, 19, 3, 11),
                LocalDateTime.of(2023, 5, 28, 22, 25),
                "IP_Nigga"));
        trips.add(new Trip("12", "City С", "City B", 1000.0,
                LocalDateTime.of(2023, 5, 20, 1, 0),
                LocalDateTime.of(2023, 5, 20, 1, 15),
                "IP_Kolomoiski"));
        trips.add(new Trip("13", "City A", "City B", 50.0,
                LocalDateTime.of(2024, 7, 10, 1, 0),
                LocalDateTime.of(2029, 11, 6, 12, 0),
                "IP_Lena"));
        trips.add(new Trip("14", "City С", "City A", 8900.0,
                LocalDateTime.of(2022, 7, 15, 23, 0),
                LocalDateTime.of(2033, 2, 24, 13, 0),
                "IP_Kernes"));
        trips.add(new Trip("15", "City A", "City C", 150.0,
                LocalDateTime.of(2023, 5, 17, 18, 6),
                LocalDateTime.of(2023, 5, 20, 13, 0),
                "IP_Den"));
        trips.add(new Trip("16", "City B", "City A", 90.0,
                LocalDateTime.of(2023, 5, 22, 13, 0),
                LocalDateTime.of(2023, 5, 24, 14, 0),
                "IP_Kazimir"));
        trips.add(new Trip("17", "City A", "City B", 120.0,
                LocalDateTime.of(2023, 5, 15, 13, 0),
                LocalDateTime.of(2023, 5, 16, 14, 0),
                "IP_Den"));
        trips.add(new Trip("18", "City C", "City A", 60.0,
                LocalDateTime.of(2023, 5, 18, 4, 0),
                LocalDateTime.of(2023, 11, 6, 12, 0),
                "IP_Lena"));
        trips.add(new Trip("19", "City B", "City C", 12220.0,
                LocalDateTime.of(2023, 7, 11, 1, 0),
                LocalDateTime.of(2023, 8, 6, 12, 0),
                "IP_Vlados"));
        trips.add(new Trip("20", "City A", "City B", 1.0,
                LocalDateTime.of(2023, 5, 18, 3, 11),
                LocalDateTime.of(2023, 5, 21, 22, 25),
                "IP_Nigga"));
        trips.add(new Trip("21", "City A", "City B", 120.0,
                LocalDateTime.of(2023, 5, 21, 9, 0),
                LocalDateTime.of(2023, 5, 24, 18, 0),
                "IP_Katkovskie"));
        return trips;
    }
}
