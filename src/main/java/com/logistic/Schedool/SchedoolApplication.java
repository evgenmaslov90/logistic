package com.logistic.Schedool;

import com.logistic.Schedool.Entity.Trip;
import com.logistic.Schedool.Service.CreatingTrips;
import com.logistic.Schedool.Service.TripUtil;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.joining;

@SpringBootApplication
public class SchedoolApplication {

    public static void main(String[] args) {

        //Все поездки
        List<Trip> currentTrips = CreatingTrips.createTrips();

        //Пустая коллекция для отсортированных
        List<Trip> sortedTrips;

        //Лист для критериев
        List<String> sortingCriteria = List.of("price", "startTime");


        TripUtil tripUtil = new TripUtil("IP_Katkovskie", "City A", "City B", sortingCriteria);

        sortedTrips = tripUtil.prioritizeAndSortTrips(currentTrips);

        System.out.println(sortedTrips.stream()
                .map(Objects::toString)
                .collect(joining(" \n")));
    }

}
