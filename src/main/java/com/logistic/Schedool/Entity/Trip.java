package com.logistic.Schedool.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
@Builder
@EqualsAndHashCode
//Сущность поездка
public class Trip {

    String id;
    String startCity;
    String endCity;
    Double price;
    LocalDateTime startTime;
    LocalDateTime endTime;
    String carrierName;
}
